import React from 'react';

function ArrowDown({ onClickDown }) {
  return (
    <div onClick={onClickDown}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        x="0"
        y="0"
        enableBackground="new 0 0 100 100"
        viewBox="0 0 100 125"
        width="10"
        fill="#00a997"
      >
        <switch>
          <g>
            <path d="M2.5 31.3c0-2.6 1-5.1 2.9-7.1 3.9-3.9 10.3-3.9 14.2 0L50 54.5l30.4-30.4c3.9-3.9 10.3-3.9 14.2 0 3.9 3.9 3.9 10.3 0 14.2L57.1 75.8c-1.9 1.9-4.4 2.9-7.1 2.9s-5.2-1.1-7.1-2.9L5.4 38.4c-1.9-2-2.9-4.6-2.9-7.1z"></path>
          </g>
        </switch>
      </svg>
    </div>
  );
}

export default ArrowDown;
