import React from 'react';
import styled from 'styled-components';

function Button({ text, onClick, selected }) {
  return (
    <ButtonContainer selected={selected} onClick={onClick}>
      {text}
    </ButtonContainer>
  );
}

export default Button;

const ButtonContainer = styled.div`
  padding: 10px;
  background-color: ${({ selected }) => (selected ? '#ffb135' : 'white')};
  color: ${({ selected }) => (selected ? 'white' : '#00A997')};
  border: ${({ selected }) =>
    selected ? '1px solid #ffb135 ' : '1px solid #00A997 '};
  font-weight: bold;
  width: 200px;
  border-radius: 30px;
  margin: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
