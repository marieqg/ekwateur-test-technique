import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import DataContainer from './DataContainer';
import FilterBar from './FilterBar';
import moment from 'moment';
import Button from './Button';
import MonthlyEvolution from './MonthlyEvolution';

function Dashboard() {
  const [dataElectricity, setDataElectricity] = useState();
  const [dataGas, setDataGas] = useState();
  const [filterSelected, setFilterSelected] = useState('electricity');
  const [yearSelected, setYearSelected] = useState('all');
  const [displayMonthly, setDisplayMonthly] = useState(false);
  const [resultToDisplay, setResultToDisplay] = useState([]);

  // Equivalent of componentDidMount, Calls of the API when the component is mounting

  useEffect(() => {
    fetch('https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/1/gas')
      .then((response) => response.json())
      .then((data) => setDataGas(data));
    fetch(
      'https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/2/electricity'
    )
      .then((response) => response.json())
      .then((data) => setDataElectricity(data));
  }, []);

  // Function to display a result according to a type of energy
  const getResultToDisplay = (
    dataElectricity,
    dataGas,
    filterSelected,
    yearSelected
  ) => {
    switch (filterSelected) {
      case 'electricity':
        let resultElectricity =
          yearSelected !== 'all'
            ? dataElectricity?.filter((oneElement) =>
                moment(oneElement?.createdAt).isSame(yearSelected, 'year')
              )
            : dataElectricity;
        return resultElectricity;
      case 'gas':
        let resultGas =
          yearSelected !== 'all'
            ? dataGas?.filter((oneElement) =>
                moment(oneElement?.createdAt).isSame(yearSelected, 'year')
              )
            : dataGas;
        return resultGas;
      default:
    }
  };

  // Call the display data function when the year selected or the filter changes or the data itself
  useEffect(() => {
    if (dataElectricity && dataGas && filterSelected && yearSelected) {
      let _resultToDisplay = getResultToDisplay(
        dataElectricity,
        dataGas,
        filterSelected,
        yearSelected
      );
      _resultToDisplay && setResultToDisplay(_resultToDisplay);
    }
  }, [filterSelected, dataElectricity, dataGas, yearSelected]);

  // Function to analyse which years are included in the data, and render an array of years
  const getYears = (dataElectricity, dataGas) => {
    let result = [];
    let data = dataElectricity?.concat(dataGas);

    data &&
      data.map((one) => {
        let year = moment(one?.createdAt).format('YYYY');
        if (!result.find((element) => element === year)) {
          result.push(year);
        }
        return result;
      });
    return result;
  };

  return (
    <Container>
      <FilterBar
        filterSelected={filterSelected}
        setFilterSelected={setFilterSelected}
        years={getYears(dataElectricity, dataGas)}
        setYearSelected={setYearSelected}
        yearSelected={yearSelected}
      />
      <DataContainer
        resultToDisplay={resultToDisplay}
        setResultToDisplay={setResultToDisplay}
        filterSelected={filterSelected}
      />
      <Button
        selected={displayMonthly}
        text={!displayMonthly ? 'Evolution par mois ' : 'Fermer'}
        onClick={() => {
          setDisplayMonthly((displayMonthly) => !displayMonthly);
        }}
      />
      {displayMonthly && <MonthlyEvolution data={resultToDisplay} />}
    </Container>
  );
}

export default Dashboard;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
