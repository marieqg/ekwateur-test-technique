import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import OrderArrows from './OrderArrows';

function DataContainer({
  resultToDisplay,
  setResultToDisplay,
  filterSelected,
}) {
  // Function to order data according to a specify field and direction
  const orderResultToDisplay = (data, order, field) => {
    let orderedResult = [];
    // for the field other than dates that needs their Strings to be converted into Numbers
    if (field !== 'createdAt') {
      orderedResult =
        order === 'up'
          ? data.sort((a, b) => parseInt(b[field]) - parseInt(a[field]))
          : data.sort((a, b) => parseInt(a[field]) - parseInt(b[field]));
    } else {
      // because the date field needs to be compared because of its ISO format
      orderedResult =
        order === 'up'
          ? data.sort((a, b) => b.createdAt.localeCompare(a.createdAt))
          : data.sort((a, b) => a.createdAt.localeCompare(b.createdAt));
    }

    orderedResult.length > 0 &&
      setResultToDisplay((orderedResult) => [...orderedResult]);
  };

  return (
    <DataContainerMainContainer>
      {resultToDisplay.length >= 1 && (
        <table>
          {/* Head of the Table  */}
          <thead>
            <tr>
              {/* Common to both energy  */}
              <StyledTdHeadColumn>
                <TextStyledTdHeadColumn>
                  <p>Date</p>
                  <OrderArrows
                    onClickDown={() => {
                      orderResultToDisplay(
                        resultToDisplay,
                        'down',
                        'createdAt'
                      );
                    }}
                    onClickUp={() => {
                      orderResultToDisplay(resultToDisplay, 'up', 'createdAt');
                    }}
                  />
                </TextStyledTdHeadColumn>
              </StyledTdHeadColumn>
              {/* Only for electricity  */}
              {filterSelected === 'electricity' ? (
                <>
                  <StyledTdHeadColumn>
                    <TextStyledTdHeadColumn>
                      <p>Heures Pleines</p>
                      <OrderArrows
                        onClickDown={() => {
                          orderResultToDisplay(
                            resultToDisplay,
                            'down',
                            'indexHigh'
                          );
                        }}
                        onClickUp={() => {
                          orderResultToDisplay(
                            resultToDisplay,
                            'up',
                            'indexHigh'
                          );
                        }}
                      />
                    </TextStyledTdHeadColumn>
                  </StyledTdHeadColumn>
                  <StyledTdHeadColumn>
                    <TextStyledTdHeadColumn>
                      <p>Heures Creuses</p>
                      <OrderArrows
                        onClickDown={() => {
                          orderResultToDisplay(
                            resultToDisplay,
                            'down',
                            'indexLow'
                          );
                        }}
                        onClickUp={() => {
                          orderResultToDisplay(
                            resultToDisplay,
                            'up',
                            'indexLow'
                          );
                        }}
                      />
                    </TextStyledTdHeadColumn>
                  </StyledTdHeadColumn>
                </>
              ) : (
                // Only for Gas
                <StyledTdHeadColumn>
                  <TextStyledTdHeadColumn>
                    <p>Consommation</p>
                    <OrderArrows
                      onClickDown={() => {
                        orderResultToDisplay(
                          resultToDisplay,
                          'down',
                          'indexHigh'
                        );
                      }}
                      onClickUp={() => {
                        orderResultToDisplay(
                          resultToDisplay,
                          'up',
                          'indexHigh'
                        );
                      }}
                    />
                  </TextStyledTdHeadColumn>
                </StyledTdHeadColumn>
              )}
            </tr>
          </thead>
          <tbody>
            {resultToDisplay?.map((result) => {
              return (
                <tr key={result.id}>
                  <StyledTd>
                    {moment(result.createdAt).format('DD/MM/YYYY')}
                  </StyledTd>
                  <StyledTd> {result.indexHigh}</StyledTd>
                  {result.indexLow && <StyledTd> {result.indexLow}</StyledTd>}
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
      {/* Display in case there is no data  */}
      {resultToDisplay.length === 0 && (
        <TextContainer>
          Pas de résultats à afficher pour cette année. Veuillez en sélectionner
          une autre...
        </TextContainer>
      )}
    </DataContainerMainContainer>
  );
}

export default DataContainer;

const DataContainerMainContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  align-items: center;
  justify-content: center;
  margin: 20px 0px;
`;
const TextContainer = styled.div`
  padding: 10px;
`;

const StyledTd = styled.td`
  text-align: center;
`;
const StyledTdHeadColumn = styled(StyledTd)`
  font-weight: bold;
  color: #00a997;
  padding: 5px 5px;
`;

const TextStyledTdHeadColumn = styled.div`
  display: flex;
  flex-direction: row;
  p {
    padding-right: 10px;
  }
`;
