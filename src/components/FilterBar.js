import React from 'react';
import styled from 'styled-components';
import Button from './Button';

function FilterBar({
  filterSelected,
  setFilterSelected,
  setYearSelected,
  yearSelected,
  years,
}) {
  return (
    <FilterBarContainer>
      <Button
        selected={filterSelected === 'electricity' && true}
        text="Electricité"
        onClick={() => {
          setFilterSelected('electricity');
        }}
      />
      <Button
        text="Gaz"
        selected={filterSelected === 'gas' && true}
        onClick={() => {
          setFilterSelected('gas');
        }}
      />
      <StyledSelect
        value={yearSelected}
        onChange={(event) => {
          setYearSelected(event.target.value);
        }}
      >
        {years.map(
          (oneYear, index) =>
            oneYear !== 'Invalid date' && (
              <option key={index} value={oneYear}>
                {oneYear}
              </option>
            )
        )}
        <option key={'all'} value={'all'}>
          Toutes les années
        </option>
      </StyledSelect>
    </FilterBarContainer>
  );
}

export default FilterBar;

const FilterBarContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100vw;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;

const StyledSelect = styled.select`
  border: 2px solid red;
  background-color: white;
  color: #00a997;
  border: 1px solid #00a997;
  font-weight: bold;
  border-radius: 30px;
  margin: 5px;
  text-align: center;
  padding: 10px;
`;
