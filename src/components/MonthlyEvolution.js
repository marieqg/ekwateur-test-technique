import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

function MonthlyEvolution({ data }) {
  // function allowing to calculate the monthly consumption
  const sumMonthlyConsumption = (data) => {
    let orderedData = data.sort((a, b) =>
      b.createdAt.localeCompare(a.createdAt)
    );
    let months = [];

    orderedData.map((data) => {
      let dataYear = parseInt(moment(data.createdAt).format('YYYY'));
      let dataMonth = parseInt(moment(data.createdAt).format('M'));
      let dataIndex = data.indexLow
        ? data.indexHigh + data.indexLow
        : data.indexHigh;

      let indexOfMonthFounded = months.findIndex(
        (element) => element.month === dataMonth && element.year === dataYear
      );

      if (indexOfMonthFounded !== -1) {
        months[indexOfMonthFounded] = {
          ...months[indexOfMonthFounded],
          total: months[indexOfMonthFounded].total + dataIndex,
        };
      } else {
        months.push({
          year: parseInt(dataYear),
          month: parseInt(dataMonth),
          total: dataIndex,
        });
      }
      return months;
    });

    return months;
  };

  // function to calculate the evolution between two following months
  const getMonthlyEvolution = (data) => {
    let result = [];
    const monthlyConsumption = sumMonthlyConsumption(data);
    monthlyConsumption.map((oneMonth) => {
      let indexOfNextMonth = monthlyConsumption.findIndex(
        (element) =>
          element.month === oneMonth.month + 1 && element.year === oneMonth.year
      );
      if (indexOfNextMonth >= 0) {
        let nextMonth = monthlyConsumption[indexOfNextMonth] || null;
        result.push({
          monthOne: oneMonth.month,
          monthTwo: oneMonth.month + 1,
          year: oneMonth.year,
          evolution:
            ((nextMonth.total - oneMonth.total) / oneMonth.total) * 100,
        });
      }
      return result;
    });
    return result;
  };

  let dataToDisplay = getMonthlyEvolution(data);
  return (
    <MonthlyEvolutionContainer>
      {dataToDisplay.length >= 1 ? (
        <table>
          {/* Head of the Table  */}
          <thead>
            <tr>
              <StyledTdHeadColumn> Mois </StyledTdHeadColumn>
              <StyledTdHeadColumn> Evolution </StyledTdHeadColumn>
            </tr>
          </thead>
          {/* Body of the Table  */}
          <tbody>
            {dataToDisplay.map((oneMonth, index) => {
              return (
                <tr key={index}>
                  <StyledTd>
                    De {oneMonth.monthOne}/{oneMonth.year} à {oneMonth.monthTwo}
                    /{oneMonth.year}
                  </StyledTd>
                  <StyledTd>
                    {Number.parseFloat(oneMonth.evolution).toFixed(2)}%
                  </StyledTd>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        // In case there is not enought data to have a monthly evolution
        <div>
          Les données ne sont pas suffisantes pour obtenir une évolution
          mensuelle
        </div>
      )}
    </MonthlyEvolutionContainer>
  );
}

export default MonthlyEvolution;

const MonthlyEvolutionContainer = styled.div`
  margin: 20px 0px;
`;

const StyledTd = styled.td`
  text-align: center;
`;
const StyledTdHeadColumn = styled(StyledTd)`
  font-weight: bold;
  color: #00a997;
  padding: 5px 5px;
`;
