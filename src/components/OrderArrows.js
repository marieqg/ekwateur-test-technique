import React from 'react';
import styled from 'styled-components';
import ArrowDown from '../assets/icons/ArrowDown';
import ArrowUp from '../assets/icons/ArrowUp';

function Order({ onClickUp, onClickDown }) {
  return (
    <Container>
      <ArrowUp onClickUp={onClickUp} />
      <ArrowDown onClickDown={onClickDown} />
    </Container>
  );
}

export default Order;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
